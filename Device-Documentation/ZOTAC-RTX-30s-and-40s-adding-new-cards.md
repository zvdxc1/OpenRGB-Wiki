Three files will need to edited, each adding one line of code.

The first is to add the card to OpenRGB's list of known PCI devices, this is in:

`pci_ids/pci_ids.h`

Next associates this card with the Zotac controller, this is in:

`Controllers/ZotacV2GPUController/ZotacV2GPUControllerDetect.cpp`

And the last specifies the LED configuration based on the card version, this is in:

`Controllers/ZotacV2GPUController/RGBController_ZotacV2GPU.cpp`

The following information will need to be gathered:

- Device_ID
- Sub-Device_ID
- I2C_Address
- Controller_Version
- LED_Configuration

For reference this guide will follow the inclusion of the ZOTAC GAMING GeForce RTX 3080 Trinity OC LHR 12GB.

---

<details>
<summary>

### Gathering Information

</summary>

<details>
<summary>

#### Windows

</summary>

 For Windows this information will require the Zotac Firestorm app, and NvAPISpy. Zotac's app can be found on their website, NvAPISpy can be found here:

https://gitlab.com/OpenRGBDevelopers/NvAPISpy

NvAPISpy may need to be configured globally. For the Device IDs, open CMD or PowerShell and execute the following:

```plaintext
wmic path win32_VideoController get PNPDeviceID
```

For the reference card, the output is:

> ```plaintext
> PCI\VEN_10DE&DEV_220A&SUBSYS_B61219DA&REV_A1\4&2283F625&0&0019
> ```

What's needed are the four characters after DEV\_ (for this "220A"), and the four after SUBSYS\_ (in this case "B612"), so:

* Device_ID := 220A
* Sub-Device_ID := B612

Now, with NvAPISpy configured, run the Firestorm app. While in the app, switch to the spectra tab and count the number of independently configurable zones, and note whether or not an external RGB strip is supported. The reference card has two zones, and does not support an external strip, so the configuration is:

* LED_Configuration := { 2, false }

Just starting the Firestorm app is sufficient for collecting the rest of the information. In the NvAPISpy log, find when the Firestorm app first polls the card for its identifier, and the response. This should be the first and second lines. For the reference card these are:

> ```plaintext
> NvAPI_I2CWrite:  Dev: 0x49 RegSize: 0x00 Reg: Size: 0x08 Data: 0xA0 0xF1 0x00 0x00 0x00 0x00 0x00 0x00
> NvAPI_I2CRead:  Dev: 0x49 RegSize: 0x01 Reg: 0xA0 Size: 0x20 Data: 0x4E 0x36 0x31 0x32 0x45 0x2D 0x31 0x30 0x31 0x31 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00
> ```

The I2C address is after Dev: in both the Write and the Read. For the reference card this is:

* I2C_Address := 0x49

The first ten bytes from the Read data are the Controller Version in hexadecimal bytes. For the reference card:

> ```plaintext
> 0x4E 0x36 0x31 0x32 0x45 0x2D 0x31 0x30 0x31 0x31
> ```

This hex string will then need to be converted in unicode, there are plenty of free converters online. In this case the unicode is:

* Controller_Version := N612E-1011

</details>

<details>
<summary>

#### Linux

</summary>

<details>
<summary>

**The information needed is much easier to find using Windows, this is because Zotac does not provide a Linux version of the Firestorm app to track its communication with the card.**

</summary>

<details>
<summary>

**This point cannot be overstressed, it may be easier to configure a dual boot or setup a live install.**

</summary>

<details>
<summary>

### Writing data to an unconfirmed I2C address is required. Do not do this if you do not understand the risks.

</summary>

 For Linux this information will require i2c-tools, these will also be required to run OpenRGB, refer to configuration instructions here:

https://gitlab.com/CalcProgrammer1/OpenRGB#smbus-access-1

For the Device IDs, open a terminal and execute the following:

```plaintext
lspci -d 1002: -nnvm | head -6 | tail -n 4 && lspci -d 10DE: -nnvm | head -6 | tail -n 4
```

For the reference card, the output is:

> ```plaintext
> Vendor:	NVIDIA Corporation [10de]
> Device:	GA102 [GeForce RTX 3080 12GB] [220a]
> SVendor:	ZOTAC International (MCO) Ltd. [19da]
> SDevice:	Device [b612]
> ```

What's needed are the four bracket characters at the end of Device (for this "220a"), and the four bracketed characters at the end of SDevice (in this case "b612"), so:

* Device_ID := 220A
* Sub-Device_ID := B612

Now, with i2c-tools configured, in a terminal execute the following:

```plaintext
i2cdetect -l
```

For the reference card the output is:

> ```plaintext
> i2c-0	smbus     	SMBus PIIX4 adapter port 0 at 0b00	SMBus adapter
> i2c-1	smbus     	SMBus PIIX4 adapter port 2 at 0b00	SMBus adapter
> i2c-2	i2c       	NVIDIA i2c adapter 1 at 7:00.0  	I2C adapter
> i2c-3	i2c       	NVIDIA i2c adapter 5 at 7:00.0  	I2C adapter
> i2c-4	i2c       	NVIDIA i2c adapter 6 at 7:00.0  	I2C adapter
> i2c-5	i2c       	NVIDIA i2c adapter 7 at 7:00.0  	I2C adapter
> i2c-6	i2c       	NVIDIA i2c adapter 8 at 7:00.0  	I2C adapter
> ```

The card's RGB controller is potentially located on any one of these busses which are associated with NVIDIA. To determine which, the available addresses on each will need to be found. To do so, exectute "i2cdetect -y #" where # is the last digit of the i2c bus in question. For the reference card, looking at bus 2 this would be:

```plaintext
i2cdetect -y 2
```

The output is:

> ```plaintext
>      0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
> 00:                         -- -- -- -- -- -- -- -- 
> 10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
> 20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
> 30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
> 40: -- -- -- -- -- -- -- -- -- 49 -- -- -- -- -- -- 
> 50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
> 60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
> 70: -- -- -- -- -- -- -- --   
> ```

Which means that on i2c bus 2 there is one valid address at 0x49. To determine if this particular address is the RGB controller, send the data packet which polls for the Controller Version:

```plaintext
i2ctransfer -y 2 w8@0x49 0xA0 0xF1 0x00 0x00 0x00 0x00 0x00 0x00
```

Note this data packet packet is being sent along i2ctransfer -y #, in this case bus 2; and is being sent to address w8@####, in this case 0x49. If this is the controller, the following read will return the controller version in hexadecimal bytes. Read the response with:

```plaintext
i2ctransfer -y 2 r10@0x49
```

The r10 here indicates reading 10 bytes, whereas the previous command wrote 8 bytes with w8. The response for the reference card is:

> ```plaintext
> 0x4e 0x36 0x31 0x32 0x45 0x2d 0x31 0x30 0x31 0x31
> ```

There's a decent chance your system has xxd available, so this can be converted to unicode directly in bash with:

```plaintext
echo $(!!) | xxd -p -r && echo
```

For the reference card the output is:

> ```plaintext
> N612E-1011
> ```

Note that the middle three letters of the first block match the last three letters of the Sub-Device ID, so far this has been the case for every card. It has also been the case that every card has used an I2C Address of 0x49. The form of this output has confirmed the correct I2C Address has been identified, and the Controller Version has been found. For the reference card these are:

* I2C_Address := 0x49
* Controller_Version := N612E-1011

If the wrong address has been used, for example, bus 3 address 0x3a:

```plaintext
i2ctransfer -y 3 w8@0x3a 0xA0 0xF1 0x00 0x00 0x00 0x00 0x00 0x00
```

The unicode converted response is:

> ```plaintext
> s
> ```

So... not the RGB controller. All available NVIDIA busses and address were issued this command for the reference card, and no hardware issues were observed to result from it. Sometimes the RGB on the card turns off, but can be restarted with no issues. For the reference card both RGB Zones can be turned on static white synchronized with the following command:

```plaintext
i2ctransfer -y 2 w16@0x49 0xA0 0x01 0x00 0x00 0x00 0x00 0x02 0x00 0xFF 0xFF 0xFF 0x64 0x64 0x00 0x00 0x01
```

This will be used to determine the number of configurable zones on the card. If the card's RGB is already all white and static, then change the three 0xFF entries to something else, 0x00 indicates that particular color is completely off, so this is a relatively easy way to differentiate.

```plaintext
i2ctransfer -y 2 w16@0x49 0xA0 0x01 0x00 0x00 0x00 0x00 0x02 0x00 REDD GREN BLUE 0x64 0x64 0x00 0x00 0x01
```

Note that the zone is the byte two positions before the three RGB bytes:

```plaintext
i2ctransfer -y 2 w16@0x49 0xA0 0x01 0x00 0x00 0x00 0x00 ZONE 0x00 REDD GREN BLUE 0x64 0x64 0x00 0x00 0x01
```

The last byte is the sync flag. This will actually need to be off for this to work, so change to 0x00:

```plaintext
i2ctransfer -y 2 w16@0x49 0xA0 0x01 0x00 0x00 0x00 0x00 ZONE 0x00 REDD GREN BLUE 0x64 0x64 0x00 0x00 SYNC
```

Begin by power cycling the card's RGBs, ensure the following commands are edited to reflect the bus and address for the card in question.

```plaintext
i2ctransfer -y 2 w2@0x49 0xA0 0x00 #off
i2ctransfer -y 2 w2@0x49 0xA0 0x01 #on
```

Then begin testing zones starting at 0x00 and working up until the entire card fits whatever the new color is. After the first command is issued without a sync flag, the card may switch the other zones to a default color. If this happens, make sure the color being sent cannot be mistaken for this default color; power cycle the RGB, then begin again with a color that is different from the one first commanded and any that may have appeared. Continue until all zones are changed. This count is the number of integrated RGB zones on the card. For the reference card this process looks like:

> ```plaintext
> [me@nobara-pc ~]$ i2ctransfer -y 2 w2@0x49 0xA0 0x00 #off
> [me@nobara-pc ~]$ i2ctransfer -y 2 w2@0x49 0xA0 0x01 #on
> [me@nobara-pc ~]$ i2ctransfer -y 2 w16@0x49 0xA0 0x01 0x00 0x00 0x00 0x00 0x00 0x00 0xFF 0xFF 0xFF 0x64 0x64 0x00 0x00 0x00
> [me@nobara-pc ~]$ i2ctransfer -y 2 w2@0x49 0xA0 0x00 #off
> [me@nobara-pc ~]$ i2ctransfer -y 2 w2@0x49 0xA0 0x01 #on
> [me@nobara-pc ~]$ i2ctransfer -y 2 w16@0x49 0xA0 0x01 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0xFF 0xFF 0x64 0x64 0x00 0x00 0x00
> [me@nobara-pc ~]$ i2ctransfer -y 2 w16@0x49 0xA0 0x01 0x00 0x00 0x00 0x00 0x01 0x00 0x00 0xFF 0xFF 0x64 0x64 0x00 0x00 0x00
> ```

After the second power cycle, the color issued in the command was changed. This new command changed one zone. The second command changed the remaining lights on the card. This means there are two independent zones. To be sure of this, the process can be reversed using a new color working down through the zones.

> ```plaintext
> [me@nobara-pc ~]$ i2ctransfer -y 2 w16@0x49 0xA0 0x01 0x00 0x00 0x00 0x00 0x01 0x00 0x00 0x00 0xFF 0x64 0x64 0x00 0x00 0x00
> [me@nobara-pc ~]$ i2ctransfer -y 2 w16@0x49 0xA0 0x01 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0x00 0xFF 0x64 0x64 0x00 0x00 0x00
> ```

So, on the reference card, there are two integrated RGB zones. This card does not support an external LED strip, so the LED Configuration is:

* LED_Configuration := { 2, false }

Support for an external strip can be determined by physical inspection of the card, or checking Zotac's documentation or website.

</details>

</details>

</details>

</details>

</details>

---

<details>
<summary>

### Adding the card

</summary>

 Now to use the gathered information add support to OpenRGB. You will need the following information specific to your card:

- Device_ID
- Sub-Device_ID
- I2C_Address
- Controller_Version
- LED_Configuration

For the reference card these values are:

> * Device_ID := 220A
> * Sub-Device_ID := B612
> * I2C_Address := 0x49
> * Controller_Version := N612E-1011
> * LED_Configuration := { 2, false }

Open the OpenRGB file pci_ids/pci_ids.h and search for the Device_ID characters. There may be multiple results, look for the one under the nVidia Device IDs section which matches your card. For the reference card the line of interest is:

```plaintext
#define NVIDIA_RTX3080_12G_LHR_DEV                      0x220A
```

Make note of the NVIDIA_ID being defined, this will be used later in `Controllers/ZotacV2GPUController/ZotacV2GPUControllerDetect.cpp`

For now, the first line of code to add is to `pci_ids/pci_ids.h` under the Zotac Sub-Device IDs section. Here a sub device ID will be defined for the new card, using part of the NVIDIA_ID. Replace NVIDIA with ZOTAC and replace DEV with a Zotac model identifier and \_SUB_DEV, then use the Sub-Device_ID with an 0x hex identifier. For the reference card the new line is:

```plaintext
#define ZOTAC_RTX3080_12G_LHR_TRINITY_SUB_DEV           0xB612
```

This new ZOTAC_ID, the NVIDIA_ID, and the I2C_Address will be used for the second new line, added to `Controllers/ZotacV2GPUController/ZotacV2GPUControllerDetect.cpp` to register the new card with the detector. At the bottom of the file there is a chunk of these, for the reference card, the new line will look like:

```plaintext
REGISTER_I2C_PCI_DETECTOR("ZOTAC GAMING GeForce RTX 3080 Trinity OC LHR 12GB", DetectZotacV2GPUControllers, NVIDIA_VEN, NVIDIA_RTX3080_12G_LHR_DEV, ZOTAC_SUB_VEN, ZOTAC_RTX3080_12G_LHR_TRINITY_SUB_DEV, 0x49);
```

This is all one line, note the name of the card, the NVIDIA_ID found earlier, the ZOTAC_ID created, and the I2C_Address.

Now, for the last line, in `Controllers/ZotacV2GPUController/RGBController_ZotacV2GPU.cpp` the ZOTAC_V2_GPU_CONFIG map defined near the beginning of the file will need a new entry. For this card the new line is:

```plaintext
{ "N612E-1011", { 2, false } },  // ZOTAC GAMING GeForce RTX 3080 Trinity OC LHR 12GB
```

Note the Controller_Version, LED_Configuration, and a comment with the card name. This entry does not need to be unique, if there is already another entry with the same Controller_Version and LED_Configuration just add the card name to the comment.

</details>

